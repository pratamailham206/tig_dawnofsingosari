# Dawn of Singasari

## About

Game about history of Ken Arok

Created with Unity

## Genre

Action RPG

## Platform

Microsoft Desktop

## Development Team
- Ilham Pratama
- Sulthan Mahendra Mubarak
- Faidurrohman

## Screenshot

![](images/111.PNG)

![](images/112.PNG)

![](images/113.PNG)
