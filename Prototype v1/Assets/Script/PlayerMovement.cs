﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum PlayerState
{
    Idle, Walk, Attack, Stagger
}

public class PlayerMovement : MonoBehaviour
{
    public PlayerState currentState;
    public float speed;
    public float health;
    public FloatValue playerHealth;
    private Rigidbody2D playerRigidBody;
    private Vector3 change;
    private Animator animator;
    public VectorValue startpos;
    public string LoadingScene;
    // Start is called before the first frame update

    private void Awake()
    {
        health = playerHealth.initialValue;
    }

    void Start()
    {
        currentState = PlayerState.Walk;
        animator = GetComponent<Animator>();
        playerRigidBody = GetComponent<Rigidbody2D>();
        transform.position = startpos.InitialValue;
    }

    // Update is called once per frame
    void Update()
    {
        change = Vector3.zero;
        change.x = Input.GetAxisRaw("Horizontal");
        change.y = Input.GetAxisRaw("Vertical");
        if (Input.GetButtonDown("Attack") && currentState != PlayerState.Attack && currentState != PlayerState.Stagger)
        {
            StartCoroutine(AttackCo());
        }
        else if (currentState == PlayerState.Walk || currentState == PlayerState.Idle)
        {
            UpdateAnimation();
        }
       
        
    }

    private IEnumerator AttackCo()
    {
        animator.SetBool("Attacking", true);
        currentState = PlayerState.Attack;
        yield return null;
        animator.SetBool("Attacking", false);
        yield return new WaitForSeconds(0.3f);
        currentState = PlayerState.Walk;
    }

    void UpdateAnimation()
    {
        if (change != Vector3.zero)
        {
            MoveChar();
            animator.SetFloat("MoveX", change.x);
            animator.SetFloat("MoveY", change.y);
            animator.SetBool("Moving", true);

        }
        else
        {
            animator.SetBool("Moving", false);
        }
    }

    void MoveChar()
    {
        playerRigidBody.MovePosition(
           transform.position+change*speed*Time.deltaTime
        );
    }

    private void takeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            SceneManager.LoadScene(LoadingScene, LoadSceneMode.Single);
        }
    }

    public void Knock (float knockTime, float damage)
    {
        StartCoroutine(KnockCo (knockTime));
        takeDamage(damage);
    }

    private IEnumerator KnockCo(float knockbackTime)
    {
        if (playerRigidBody != null)
        {
            yield return new WaitForSeconds(knockbackTime);
            playerRigidBody.velocity = Vector2.zero;
            currentState = PlayerState.Idle;
            
        }
    }
}
