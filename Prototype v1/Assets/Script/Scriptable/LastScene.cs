﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastScene : ScriptableObject
{
    public string sceneLastAccessed;
    public string sceneNow;
}
