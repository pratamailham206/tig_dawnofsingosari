﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : Enemy {
    // Start is called before the first frame update

    private Rigidbody2D myRigidbody;
    public Transform target;
    public float chaseradius;
    public float attackradius;
    public Transform homeposition;

    void Start()
    {
        currentstate = EnemyState.idle;
        myRigidbody = GetComponent<Rigidbody2D>();
        
        target = GameObject.FindWithTag("Player").transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        checkDistance();
    }

    void checkDistance()
    {
        if (Vector3.Distance(target.position, transform.position) <= chaseradius && Vector3.Distance(target.position, transform.position) >= attackradius)
        {
            if (currentstate == EnemyState.idle || currentstate == EnemyState.walk && currentstate != EnemyState.stagger)
            {

                Vector3 temp = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);

                myRigidbody.MovePosition(temp);
                changeState(EnemyState.walk);
            }

        }
    }

    private void changeState(EnemyState newState)
    {
        if (currentstate != newState)
        {
            currentstate = newState;
        }
    }
}
