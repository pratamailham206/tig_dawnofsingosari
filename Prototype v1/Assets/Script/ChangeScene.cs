﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public string LoadingScene;
    public Vector2 playerpos;
    public VectorValue storepos;

    // Start is called before the first frame update
    void Start()
    {
       
    }
        
    // Update is called once per frame
    void Update()
    {
        
    }

  
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            storepos.InitialValue = playerpos; 
            SceneManager.LoadScene(LoadingScene, LoadSceneMode.Single);
        }
    }
}
