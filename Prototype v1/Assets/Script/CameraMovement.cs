﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform targetplayer;
    public float smooth;
    public Vector2 maxPos;
    public Vector2 minPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (transform.position != targetplayer.position)
        {
            Vector3 targetposition = new Vector3(targetplayer.position.x, targetplayer.position.y,transform.position.z);

            targetposition.x = Mathf.Clamp(targetposition.x, minPos.x, maxPos.x);
            targetposition.y = Mathf.Clamp(targetposition.y, minPos.y, maxPos.y);


            transform.position = Vector3.Lerp(transform.position, targetposition, smooth);
        }
    }
}
