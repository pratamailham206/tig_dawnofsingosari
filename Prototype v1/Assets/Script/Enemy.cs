﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState
{
    idle,
    walk,
    attack,
    stagger
}

public class Enemy : MonoBehaviour
{
    public EnemyState currentstate;
    public float health;
    public FloatValue maxHealth;
    public int baseDMG;
    public float moveSpeed;

    private void Awake()
    {
        health = maxHealth.initialValue;
    }

    private void takeDamage(float damage)
    {
        health -= damage;
        if(health <= 0)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void Knock(Rigidbody2D Enemy, float knockbackTime, float damage)
    {
        StartCoroutine(KnockCo(Enemy,knockbackTime));
        takeDamage(damage);
    }

    private IEnumerator KnockCo(Rigidbody2D Enemy, float knockbackTime)
    {
        if (Enemy != null)
        {
            yield return new WaitForSeconds(knockbackTime);
            Enemy.velocity = Vector2.zero;
            currentstate = EnemyState.idle;
            
        }
    }

    private void Start()
    {
       health = maxHealth.initialValue;
    }
}
